# README #

This is a default `README.md` for `envoy-gateway`.

### How to get started with this repository ###

Commits directly to `master` are not allowed.

To commit changes to `master`:

* create a fork or branch of `envoy-gateway/master`
* commit changes to your fork / branch
* submit a pull request to merge your changes up to `envoy-gateway/master` once changes are approved

This repository contains the configuration templates for the Envoy Front Proxy that we are exposing to the public as our API gateway. To make changes to Envoy, you must edit one of the 10-app-configmap.yaml file respective to your environment. 

The purpose of the gateway is to perform authentication for each incoming request. This is done through the jwt_authn filter in the config. If a route has not been whitelisted to skip authentication, it will default to calling PingFederate to verify the token's signature. 

After successful authentication, the gateway will use information from the request to route to the appropriate service. 

For GRPC requests, we route according to the service name. This is a combination of the proto file's package + service name (i.e. omnitracs.platform.businessaccount.service.v1.BusinessAccountService for business account service). The incoming grpc/http2 request will use that string as its route suffixed with the method name being called (i.e. /omnitracs.platform.businessaccount.service.v1.BusinessAccountService/CreateBusinessAccount). With that route, Envoy can appropriately route the request to the business account service Envoy Proxy.

For REST requests, we route according to the resource prefix. If we have a route defined in one of our services, i.e. /v1/businessAccounts, we must tell Envoy how to map that route to a service. We do this by adding route prefix/fragment. So in the previous example our request would look like this: https://<envoy-gateway-URL>/business-account-service/v1/businessAccounts. Envoy uses the /business-account-service/ fragment of the route to match to the business account service Envoy Proxy. We then perform a prefix rewrite to remove that fragment to exactly match the route defined in our proto files. We have to do this because we cannot guarantee that a URI maps to a service 100% of the time. For example, in vehicle service we have some routes that prefix with /v1/businessAccounts just as in business accounts. How would Envoy Gateway know whether to call vehicle or business account service? We could theoretically map the entire URI but that would be very difficult to maintain with every route. The simplest solution was to add a prefix per service.

 
